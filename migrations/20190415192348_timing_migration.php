<?php


use Phinx\Migration\AbstractMigration;

class TimingMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->execute('
            CREATE TABLE `timing` (
                `id` INT(11) NOT NULL AUTO_INCREMENT,
                `date_of_departure` DATE  NOT NULL,
                `date_of_arrival` DATE  NOT NULL,
                `date_of_return` DATE  NOT NULL,
                `courier_id` INT NOT NULL,
                `region_id` INT NOT NULL,
                 PRIMARY KEY(`id`),
            FOREIGN KEY (courier_id) REFERENCES couriers(id) ON UPDATE CASCADE ON DELETE RESTRICT,
            FOREIGN KEY (region_id) REFERENCES regions(id) ON UPDATE CASCADE ON DELETE RESTRICT
            )
        ');
    }
}
