<?php


use Phinx\Seed\AbstractSeed;

class CouriersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        $data = [];
        for($i=1; $i<=100; $i++) {
            $data[]=[
                'id' => $i,
                'fio' => $faker->name
            ];
        }
        $posts = $this->table('couriers');
        $posts->insert($data)
            ->save();

    }
}
