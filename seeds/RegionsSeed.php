<?php


use Phinx\Seed\AbstractSeed;

class RegionsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            [
                'name'    => 'Санкт-Петербург',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Уфа',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Нижний Новгород',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Владимир',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Кострома',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Екатеринбург',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Ковров',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Воронеж',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Самара',
                'days_in_transit' => rand(1, 10),
            ],
            [
                'name'    => 'Астрахань',
                'days_in_transit' => rand(1, 10),
            ],
        ];
        $posts = $this->table('regions');
        $posts->insert($data)
            ->save();
    }
}
