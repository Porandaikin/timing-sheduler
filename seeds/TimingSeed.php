<?php

use App\ConfigInstance;
use App\Models\Courier;
use App\Models\Region;
use App\Models\Timing;
use App\Repository\TaskRepository;
use App\Service\TimingService;
use Phinx\Seed\AbstractSeed;

class TimingSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $entityManager =  ConfigInstance::getEntityManager();
        $faker = \Faker\Factory::create();
        /** @var TaskRepository $taskRepository */
        $courierRepository = ConfigInstance::getEntityManager()->getRepository(Courier::class);
        $couriers = $courierRepository->findAll();
        for($i = 0; $i < 20; $i++) {
            /** @var Courier $courier */
            $courier = $couriers[$i];
            $regionRepository = ConfigInstance::getEntityManager()->getRepository(Region::class);
            $regions = $regionRepository->findAll();
            /** @var Region $region */
            $region = $regions[array_rand($regions)];
            $dateOfDeparture = $faker->dateTimeBetween($startDate = '2018-03-01 01:00:00', $endDate = 'now');
            [$dateOfArrival, $dateOfReturn] = TimingService::getDatetimeMovement($dateOfDeparture, $region->getDaysInTransit());
            $timing = new Timing();
            $timing->setCourier($courier)
                ->setDateOfArrival($dateOfArrival)
                ->setDateOfDeparture($dateOfDeparture)
                ->setDateOfReturn($dateOfReturn)
                ->setRegion($region);
            $entityManager->persist($timing);
            $entityManager->flush($timing);
        }
    }
}
