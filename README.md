1. Создать БД _CREATE DATABASE `vit` CHARACTER SET utf8 COLLATE utf8_general_ci;_
2. /etc/hosts добавить url vit.local
3. настройте nginx, пример файла конфиг. vit.conf.example. Необходимо подкорретировать под свою систему. Для apache есть конфиг .htaccess
4. настройте файлы phinx.yml и .env под свою бд
5. установите node, npm
6. выполните команду npm run init