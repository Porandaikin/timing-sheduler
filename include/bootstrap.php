<?php

use App\ConfigInstance;

$httpMethod = $_SERVER['REQUEST_METHOD'] ?? '';
$uri = $_SERVER['REQUEST_URI'] ?? '';
ConfigInstance::initWhoops();

ConfigInstance::initDotenv();

$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $route) {
    $route->addRoute('GET', '/', 'IndexController@list');
    $route->addRoute('POST', '/getTimingSheduler/', 'IndexController@getTimingSheduler');
    $route->addRoute('GET', '/create/', 'IndexController@create');
    $route->addRoute('POST', '/recruitmentCouriers/', 'IndexController@recruitmentCouriers');
    $route->addRoute('POST', '/addCourier/', 'IndexController@addCourier');
});
