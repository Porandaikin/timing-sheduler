export class Site {
    constructor() {
        this.page = 1;
        this._reloadTimingSheduler();
        $("#filter_timing").click(() => {
            this._filter();
        });
        $("#addTripButton").click(() => {
            this._uploadCreateForm();
        });
    }

    _filter() {
        this._reloadTimingSheduler();
    };

    _getStartPerion() {
        return $("#startPeriodDatepicker").val();
    };

    _getEndPerion() {
        return $("#endPeriodDatepicker").val();
    };

    _showLoading() {
        $("#loading").show();
    };

    _hideLoading() {
        $("#loading").hide();
    };

    _reloadTimingSheduler() {
        this._showLoading();
        this._hideErrorMessage();
        $.post(`/getTimingSheduler/`, {
            'start_perion': this._getStartPerion(),
            'end_period': this._getEndPerion(),
            'page': this.page
        })
            .done((response) => {
                let data = JSON.parse(response);
                $("#timing-sheduler").html(data.view);
                if (data.status === false) {
                    this._showErrorMessage(data.error);
                }
                this._hideLoading();
            });
    };

    _showErrorMessage(message) {
        $("#error_message").html(message);
        $("#error_alert").show();
    };

    _hideErrorMessage() {
        $("#error_message").html('');
        $("#error_alert").hide();
    };

    actionPaginate(button) {
        this.page = $(button).data('page');
        this._reloadTimingSheduler();
        return false;
    };

    _uploadCreateForm() {
        $.get('/create/', {}).done((response) => {
            $("#createTripForm").html(response);
        });
    };

    checkCreateForm() {
        let region = parseInt($("#regionForm").val());
        let periodStart = $("#selectPeriod").val();
        if (periodStart === "") {
            this._addNotificationToCreateForm('Выберите дату отправления');
        } else if (region === 0) {
            this._addNotificationToCreateForm('Выберите регион');
        } else {
            $("#formTripNotification").hide();
            $("#loaderCouriersCreateForm").show();
            $.post(`/recruitmentCouriers/`, {
                'region': region,
                'period_start': periodStart
            })
                .done((response) => {
                    $("#loaderCouriersCreateForm").hide();
                    let data = JSON.parse(response);
                    if (data.status === true) {
                        $("#selectCouriers").html(data.view);
                        $("#selectCouriers").show();
                    } else {
                        this._addNotificationToCreateForm(data.error);
                    }
                });
        }
    };

    _addNotificationToCreateForm(notification) {
        $("#formTripNotification").show();
        $("#formTripNotification").html(notification);
        $("#selectCouriers").hide();
    };

    addTrip() {
        let region = parseInt($("#regionForm").val());
        let periodStart = $("#selectPeriod").val();
        let selectCourier = $("#selectCourier").val();
        $.post(`/addCourier/`, {
            'courier_id': selectCourier,
            'region_id': region,
            'period_start': periodStart
        })
            .done((response) => {
                let data = JSON.parse(response);
                if (data.status === false) {
                    alert(data.error);
                }
                this._filter();
            });
    };

}