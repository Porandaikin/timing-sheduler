<?php


namespace App\Models;

use Doctrine\ORM\Mapping\{Column, Id, Entity, Table};
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="regions")
 * @Entity(repositoryClass="\App\Repository\RegionRepository")
 **/
class Region
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string", length=255, name="name")
     */
    private $name;

    /**
     * @Column(type="integer", name="days_in_transit")
     */
    private $daysInTransit;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Region
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int
     */
    public function getDaysInTransit(): int
    {
        return $this->daysInTransit;
    }

    /**
     * @param int $daysInTransit
     * @return Region
     */
    public function setDaysInTransit(int $daysInTransit): self
    {
        $this->daysInTransit = $daysInTransit;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}