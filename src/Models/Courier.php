<?php


namespace App\Models;

use Doctrine\ORM\Mapping\{Column, Id, Entity, OneToMany, Table};
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Models\Timing;

/**
 * @Table(name="couriers")
 * @Entity(repositoryClass="\App\Repository\CourierRepository")
 **/
class Courier
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="string", length=255, name="fio")
     */
    private $fio;

    /**
     * @OneToMany(targetEntity="Timing", mappedBy="courier")
     */
    private $timing;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFio(): string
    {
        return $this->fio;
    }

    /**
     * @param string $fio
     * @return Courier
     */
    public function setFio(string $fio): self
    {
        $this->fio = $fio;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTiming()
    {
        return $this->timing;
    }

    /**
     * @param mixed $timing
     */
    public function setTiming($timing): void
    {
        $this->timing = $timing;
    }
}