<?php


namespace App\Models;

use Doctrine\ORM\Mapping\{Column, Id, Entity, ManyToOne, Table, OneToOne, JoinColumn};
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Table(name="timing")
 * @Entity(repositoryClass="\App\Repository\TimingRepository")
 **/
class Timing
{
    /**
     * @Id @Column(type="integer") @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="datetime", name="date_of_departure")
     */
    private $dateOfDeparture;

    /**
     * @Column(type="datetime", name="date_of_arrival")
     */
    private $dateOfArrival;

    /**
     * @Column(type="datetime", name="date_of_return")
     */
    private $dateOfReturn;

    /**
     * @ManyToOne(targetEntity="Courier", inversedBy="timing")
     * @JoinColumn(name="courier_id", referencedColumnName="id")
     */
    private $courier;

    /**
     * @OneToOne(targetEntity="Region")
     * @JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateOfDeparture(): \DateTimeInterface
    {
        return $this->dateOfDeparture;
    }

    /**
     * @param \DateTimeInterface $dateOfDeparture
     * @return Timing
     */
    public function setDateOfDeparture(\DateTimeInterface $dateOfDeparture): self
    {
        $this->dateOfDeparture = $dateOfDeparture;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateOfArrival(): \DateTimeInterface
    {
        return $this->dateOfArrival;
    }

    /**
     * @param \DateTimeInterface $dateOfArrival
     * @return Timing
     */
    public function setDateOfArrival(\DateTimeInterface $dateOfArrival): self
    {
        $this->dateOfArrival = $dateOfArrival;

        return $this;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateOfReturn(): \DateTimeInterface
    {
        return $this->dateOfReturn;
    }

    /**
     * @param $dateOfReturn
     * @return Timing
     */
    public function setDateOfReturn($dateOfReturn): self
    {
        $this->dateOfReturn = $dateOfReturn;

        return $this;
    }

    /**
     * @return Courier
     */
    public function getCourier(): Courier
    {
        return $this->courier;
    }

    /**
     * @param Courier $courier
     * @return Timing
     */
    public function setCourier(Courier $courier): self
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * @return Region
     */
    public function getRegion(): Region
    {
        return $this->region;
    }

    /**
     * @param Region $region
     * @return Timing
     */
    public function setRegion(Region $region): self
    {
        $this->region = $region;

        return $this;
    }
}