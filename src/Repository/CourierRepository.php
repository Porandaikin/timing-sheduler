<?php


namespace App\Repository;

use App\Models\Courier;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class CourierRepository extends EntityRepository
{
    public function getFreeCouriers(\DateTimeInterface $periodStart)
    {
        $sql = " 
            SELECT 
                couriers.*
            FROM couriers
            LEFT JOIN timing ON couriers.id = timing.courier_id
            where timing.id IS NULL OR timing.date_of_return < '{$periodStart->format('Y-m-d H:i:s')}'
            ORDER BY couriers.fio
        ";
        $stmt = $this->_em->getConnection()->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll();
    }
}