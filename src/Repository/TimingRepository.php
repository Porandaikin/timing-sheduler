<?php

namespace App\Repository;

use App\Models\Timing;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class TimingRepository extends EntityRepository
{
    /**
     * @param int $offset
     * @param int $limit
     * @param \DateTimeInterface $startPeriod
     * @param \DateTimeInterface $endPeriod
     * @return array
     */
    public function getFilterData(
        int $offset = 0,
        int $limit = null,
        \DateTimeInterface $startPeriod = null,
        \DateTimeInterface $endPeriod = null
    )
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('timing')
            ->from(Timing::class, 'timing');

        $this->addFilter($queryBuilder, $startPeriod, $endPeriod);
        if ($offset !== null) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit !== null) {
            $queryBuilder->setMaxResults($limit);
        }
        $queryBuilder->orderBy('timing.id');

        return $queryBuilder->getQuery()->getResult();

    }

    /**
     * @param \DateTimeInterface|null $startPeriod
     * @param \DateTimeInterface|null $endPeriod
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountRecords(\DateTimeInterface $startPeriod = null, \DateTimeInterface $endPeriod = null): int
    {
        $queryBuilder = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('count(timing.id)')
            ->from(Timing::class, 'timing');

        $this->addFilter($queryBuilder, $startPeriod, $endPeriod);

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param \DateTimeInterface|null $startPeriod
     * @param \DateTimeInterface|null $endPeriod
     */
    private function addFilter(QueryBuilder $queryBuilder, \DateTimeInterface $startPeriod = null, \DateTimeInterface $endPeriod = null)
    {

        if ($startPeriod !== null) {
            $queryBuilder->andWhere('timing.dateOfDeparture >= :startPeriod')
                ->setParameter('startPeriod', $startPeriod);
        }
        if ($endPeriod !== null) {
            $queryBuilder->andWhere('timing.dateOfArrival <= :endPeriod')
                ->setParameter('endPeriod', $endPeriod);
        }
    }
}
