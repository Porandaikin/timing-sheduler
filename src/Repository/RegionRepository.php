<?php


namespace App\Repository;

use App\Models\Region;
use App\Models\Timing;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class RegionRepository extends EntityRepository
{
    /**
     * @param $id
     * @return Region|null
     */
    public function getRegionById($id): ?Region
    {
        return $this->find($id);
    }

    public function getRegionsArray(): array
    {
        return $this
            ->createQueryBuilder('region', 'region.id')
            ->select('region')
            ->getQuery()->getArrayResult();
    }
}