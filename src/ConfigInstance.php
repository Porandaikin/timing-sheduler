<?php

namespace App;

use Doctrine\ORM\ORMException;
use Dotenv\Dotenv;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Whoops\Run as Whoops;

class ConfigInstance
{
    private static $dir = __DIR__ . '../../';
    /** @var Environment */
    private static $twig;

    /** @var EntityManager */
    private static $entityManager;

    /** @var Whoops */
    private static $whoops;

    /** @var Dotenv */
    private static $dotenv;

    public static function initWhoops()
    {
        if (empty(static::$whoops)) {
            static::$whoops = new Whoops;
            static::$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
            static::$whoops->register();
        }
    }

    public static function initDotenv()
    {
        if (empty(static::$dotenv)) {
            static::$dotenv = Dotenv::create(static::$dir);
            static::$dotenv->load();
        }
    }

    /**
     * @return Environment
     */
    public static function getTwig(): Environment
    {
        if (empty(static::$twig)) {
            $loader = new FilesystemLoader(APP_DIR . '/resources/views');
            static::$twig = new Environment($loader, [
                'cache' => APP_DIR . '/resources/cache',
            ]);
        }

        return static::$twig;
    }

    /**
     * @return EntityManager
     * @throws ORMException
     */
    public static function getEntityManager(): EntityManager
    {
        ConfigInstance::initDotenv();
        if (empty(static::$entityManager)) {
            $isDevMode = $_ENV['APP_ENV'] === 'dev' ? true : false;
            $paths = [static::$dir . '/src/Models'];
            $dbParams = [
                'driver' => $_ENV['DB_DRIVER'],
                'user' => $_ENV['DB_USER'],
                'password' => $_ENV['DB_PASS'],
                'dbname' => $_ENV['DB_DATABASE'],
                'host' => $_ENV['DB_HOST'],
                'charset' => $_ENV['CHARSET'],
            ];

            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
            static::$entityManager = EntityManager::create($dbParams, $config);
        }
        return static::$entityManager;
    }
}
