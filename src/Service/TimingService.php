<?php

namespace App\Service;

use Carbon\Carbon;
use DateTimeInterface;

class TimingService
{
    /**
     * @param DateTimeInterface $dateOfDeparture
     * @param int $daysInTransit
     * @return array
     */
    public static function getDatetimeMovement(\DateTimeInterface $dateOfDeparture, int $daysInTransit): array
    {
        $dateOfArrival = Carbon::parse($dateOfDeparture)->add($daysInTransit, 'day');
        $dateOfReturn = Carbon::parse($dateOfArrival)->add($daysInTransit, 'day');

        return [$dateOfArrival, $dateOfReturn];
    }
}