<?php

namespace App\Controllers;

use App\Models\Courier;
use App\Models\Region;
use App\Models\Timing;
use App\Repository\TimingRepository;
use App\Service\TimingService;
use Doctrine\ORM\Query;
use Faker\Provider\DateTime;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class IndexController extends BaseController
{
    const LIMIT = 5;

    /**
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function list()
    {
        echo $this->getTwig()->render('mainpage.html.twig');
    }

    public function getTimingSheduler()
    {
        $view = '';
        $status = true;
        $error = '';
        try {
            /** @var TimingRepository $timingRepository */
            $timingRepository = $this->getEntityManager()->getRepository(Timing::class);
            $page = $this->get('page', 1);
            $startPeriod = !empty($this->get('start_perion'))
                ? new \DateTime($this->get('start_perion'))
                : null;
            $endPeriod = !empty($this->get('end_period'))
                ? new \DateTime($this->get('end_period'))
                : null;
            if (!empty($startPeriod) && !empty($endPeriod) && $endPeriod < $startPeriod) {
                throw new \Exception('Выбран неправильный период');
            }
            $countRecords = $timingRepository->getCountRecords($startPeriod, $endPeriod);
            $view = $this->getTwig()->render('_timing_sheduler.html.twig', [
                'timing' => $timingRepository->getFilterData(($page - 1) * self::LIMIT, self::LIMIT, $startPeriod, $endPeriod),
                'nbPages' => ceil($countRecords / static::LIMIT),
                'currentPage' => $page,

            ]);
        } catch (\Throwable $exception) {
            $status = false;
            $error = $exception->getMessage();
        }
        echo json_encode([
            'view' => $view,
            'status' => $status,
            'error' => $error,
        ]);
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws \Doctrine\ORM\ORMException
     */
    public function create()
    {
        echo $this->getTwig()->render('create.html.twig', [
            'regions' => $this->getEntityManager()
                ->getRepository(Region::class)
                ->getRegionsArray(),
        ]);
    }

    public function recruitmentCouriers()
    {
        $view = '';
        $status = true;
        $error = '';
        try {
            $region = $this->get('region');
            $periodStart = new \DateTime($this->get('period_start'));
            if ($periodStart < (new \DateTime())) {
                throw new \Exception('Нельзя планировать поездки на прошедшие даты');
            }
            $region = $this->getEntityManager()
                ->getRepository(Region::class)
                ->getRegionById($region);
            $dayInTransit = $region->getDaysInTransit();
            [$dateOfArrival, $dateOfReturn] = TimingService::getDatetimeMovement($periodStart, $dayInTransit);
            $freeCouriers = $this->getEntityManager()
                ->getRepository(Courier::class)->getFreeCouriers($periodStart);
            $view = $this->getTwig()->render('free_coueriers.html.twig', [
                'freeCouriers' => $freeCouriers,
                'dateOfArrival' => $dateOfArrival->format('Y-m-d'),
                'dateOfReturn' => $dateOfReturn->format('Y-m-d'),
                'dayInTransit' => $dayInTransit,
            ]);
        } catch (\Throwable $exception) {
            $error = $exception->getMessage();
            $status = false;
        }

        echo json_encode([
            'view' => $view,
            'status' => $status,
            'error' => $error,
        ]);
    }

    public function addCourier()
    {
        $status = true;
        $error = '';
        try {
            $courierId = $this->get('courier_id');
            $regionId = $this->get('region_id');
            $region = $this->getEntityManager()
                ->getRepository(Region::class)
                ->find($regionId);
            $courier = $this->getEntityManager()
                ->getRepository(Courier::class)
                ->find($courierId);
            $dateOfDeparture = new \DateTime($this->get('period_start'));
            [$dateOfArrival, $dateOfReturn] = TimingService::getDatetimeMovement($dateOfDeparture, $region->getDaysInTransit());
            $timing = new Timing();
            $timing->setRegion($region)
                ->setDateOfReturn($dateOfReturn)
                ->setDateOfDeparture($dateOfDeparture)
                ->setDateOfArrival($dateOfArrival)
                ->setCourier($courier);
            $this->getEntityManager()->persist($timing);
            $this->getEntityManager()->flush($timing);

        } catch (\Throwable $exception) {
            $error = $exception->getMessage();
            $status = false;
        }
        echo json_encode([
            'status' => $status,
            'error' => $error,
        ]);
    }
}
