<?php


namespace App\Controllers;

use App\ConfigInstance;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Twig\Environment;

class BaseController
{
    /** @var array */
    private $request = [];

    public function __construct()
    {
        $this->request = $_REQUEST;
    }

    /**
     * @return EntityManager
     * @throws ORMException
     */
    final protected function getEntityManager(): EntityManager
    {
        return ConfigInstance::getEntityManager();
    }

    /**
     * @return Environment
     */
    final protected function getTwig(): Environment
    {
        return ConfigInstance::getTwig();
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    protected function get($key, $default = null)
    {
        return $this->request[$key] ?? $default;
    }
}